<?php
use yii\helpers\ArrayHelper;

Yii::setAlias('backend', dirname(__DIR__));

$params = ArrayHelper::merge(
	require(__DIR__ . '/params.php'),
	require(__DIR__ . '/params-local.php')
);

return [
	'id' => 'app-backend',
	'name' => 'ЭтоСервис',
	'basePath' => dirname(__DIR__),
	'defaultRoute' => 'admin/default/index',
	'layoutPath' => '@backend/modules/admin/views/layouts',
	'viewPath' => '@backend/modules/admin/views',
	'modules' => [
	    'users' => [
			'class' => 'backend\modules\users\Users',
			'modules' => [
			    'rbac' => [
			        'class' => 'backend\modules\users\modules\rbac\Rbac'
			    ]
			]
		],
		'admin' => [
			'class' => 'backend\modules\admin\Admin'
		],
		'blogs' => [
			'class' => 'backend\modules\blogs\Blogs',
			'modules' => [
			    'categories' => [
			        'class' => 'backend\modules\blogs\modules\categories\Categories'
			    ],
          'blocks' => [
            'class' => 'backend\modules\blogs\modules\blocks\Blocks'
          ]
			]
		],
		'galleries' => [
		    'class' => 'backend\modules\galleries\Galleries',
		    'modules' => [
			    'categories' => [
			        'class' => 'backend\modules\galleries\modules\categories\Categories'
			    ]
			]
		],
		'pages' => [
			'class' => 'backend\modules\pages\Pages'
		],
		'menu' => [
			'class' => 'backend\modules\menu\Menu'
		],
		'slider' => [
		    'class' => 'backend\modules\slider\Slider'
		],
		'tags' => [
			'class' => 'backend\modules\tags\Tags'
		],
		'materials' => [
			'class' => 'backend\modules\materials\Materials'
		]
	],
	'components' => [
		'urlManager' => [
			'rules' => [
				// Модуль Site
				'' => 'admin/default/index',
				'<_a:(login|logout)>' => 'users/default/<_a>',
				'<_m:\w+>/<_sm:\w+>/<_c:\w+>/<_a:[\w\-]+>' => '<_m>/<_sm>/<_c>/<_a>',
				'<_m:\w+>/<_c:\w+>/<_a:[\w\-]+>' => '<_m>/<_c>/<_a>',
			]
		],
		'errorHandler' => [
			'errorAction' => 'admin/default/error',
		],
		'i18n' => [
			'translations' => [
				'admin' => [
					'class' => 'yii\i18n\PhpMessageSource',
					'basePath' => '@backend/modules/admin/messages',
				],
				'blogs/categories' => [
					'class' => 'yii\i18n\PhpMessageSource',
					'basePath' => '@backend/modules/blogs/modules/categories/messages',
				]
			]
		]
	],
	'params' => isset($params['app']) ? $params['app'] : []
];