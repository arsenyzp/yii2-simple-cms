<?php
namespace backend\modules\admin\actions\crud;

use Yii;

/**
 * DeleteAction экшен удаления записи модели.
 * 
 * Пример использования:
 * ~~~
 * public function actions()
 * {
 *     'delete' => [
 *         'class' => UpdateAction::className(),
 *         'model' => Post::className()
 *     ]
 * }
 * ~~~
 */
class DeleteAction extends Action
{
	/**
	 * @inheritdoc
	 */
	public function run($id)
	{
		$model = $this->findModel($id);
		$model->delete();
		if (Yii::$app->getRequest()->isAjax) {
			return true;
		} else {
			return $this->controller->redirect(['index']);
		}
	}
}