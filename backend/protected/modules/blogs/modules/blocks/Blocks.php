<?php
/**
 * Created by PhpStorm.
 * User: arseniy
 * Date: 30/09/14
 * Time: 23:59
 */

namespace backend\modules\blogs\modules\blocks;


/**
 * Backend-модуль [[Categories]]
 */
class Blocks extends \common\modules\blogs\modules\blocks\Blocks
{
  /**
   * @inheritdoc
   */
  public $controllerNamespace = 'backend\modules\blogs\modules\blocks\controllers';
}