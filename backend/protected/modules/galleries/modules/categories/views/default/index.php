<?php
/**
 * Страница всех категорий
 * @var yii\base\View $this
 * @var backend\modules\blogs\modules\category\models\Category $dataProvider
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\grid\CheckboxColumn;
use yii\grid\SerialColumn;
use backend\modules\admin\grid\OrderingColumn;

$this->title = 'Категории галерей';
$this->params['control'] = [
    'brandLabel' => Html::encode($this->title),
    'gridId' => 'categories-grid'
];

echo GridView::widget([
    'id' => 'categories-grid',
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'class' => CheckboxColumn::classname()
        ],
        [
            'class' => OrderingColumn::className(),
            'attribute' => 'ordering'
        ],
        [
            'attribute' => 'title',
            'format' => 'html',
            'value' => function ($model) {
                return Html::a($model['title'], ['view', 'id' => $model['id']]);
            },
        ],
        [
            'attribute' => 'status_id',
            'value' => function ($model) {
                return $model->status;
            }
        ],
        [
            'class' => ActionColumn::className(),
            'header' => 'Управление'
        ]
    ]
]);