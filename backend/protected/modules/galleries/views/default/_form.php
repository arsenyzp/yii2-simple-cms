<?php
/**
 * @var yii\web\View $this
 * @var User $model
 * @var yii\widgets\ActiveForm $form
 */

use common\modules\users\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\modules\tags\widgets\tags\Tags;
use common\extensions\fileapi\FileAPI;

$form = ActiveForm::begin([
    'id' => 'gallery-form',
  	'enableClientValidation' => false,
  	'enableAjaxValidation' => true,
  	'validateOnChange' => false
]); ?>
  <div class="row">
        <div class="col-sm-12">
            <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Обновить', [
                'class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right'
            ]); ?>
        </div>
    </div>
<?= $form->field($model, 'title') .
    $form->field($model, 'category_id')->dropDownList($categoryArray, [
        'prompt' => 'Выберите категорию'
     ]) .
     $form->field($model, 'status_id')->dropDownList($statusArray, [
        'prompt' => 'Выберите статус'
     ]) .
     $form->field($model, 'ordering');

    echo $form->field($imageModel, 'name')->widget(FileAPI::className(), [
        'settings' => [
            'url' => Url::toRoute('/galleries/images/upload-temp'),
            'multiple' => true,
        ],
        'index' => (!$model->isNewRecord && $model->images) ? count($model->images) : 0
    ]);

    if (!$model->isNewRecord && $model->images) {
          echo Html::tag('h3', $model->getAttributeLabel('items'));
          foreach ($model->images as $key => $image) {
               echo '<div class="gallery-form-image row">' .
                        '<div class="col-sm-2">' .
                            '<div class="gallery-form-image-delete gallery-form-image-delete-link" data-url="' . Url::toRoute(['/galleries/images/delete', 'id' => $image['id']]) . '">' .
                                '<span class="glyphicon glyphicon-trash"></span>' .
                            '</div>' .
                            Html::img($image->url, ['alt' => $image['name'], 'class' => 'img-responsive']) .
                        '</div>' .
                        '<div class="col-sm-10">' .
                            $form->field($image, '[' . $key . ']title') .
                            $form->field($image, '[' . $key . ']last_url') .
                            $form->field($image, '[' . $key . ']snippet')->textarea() .
                            $form->field($image, '[' . $key . ']description')->textarea() .
                            $form->field($image, '[' . $key . ']ordering') .
                            $form->field($image, '[' . $key . ']tagArray')->widget(Tags::className(), [
                              'settings' => [
                                'width' => '100%'
                              ]
                            ]) .
                        '</div>' .
                    '</div>';
          }
          echo '<div class="clearfix"></div>';
          $this->registerJs("jQuery(document).on('click', '.gallery-form-image-delete-link', function(evt) {
               evt.preventDefault();
               var image = jQuery(this).parents('.gallery-form-image');
               jQuery.ajax({
                    url : jQuery(this).data('url'),
                    type : 'POST',
                    success : function(data,  textStatus, xhr) {
                         image.remove();
                    }
               });
          });");
    }

    echo Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Обновить', [
      'class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right'
    ]);
ActiveForm::end();
