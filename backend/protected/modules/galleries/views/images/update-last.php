<?php
/**
 * Страница категории
 * @var yii\base\View $this
 * @var backend\modules\galleries\models\Gallery $model
 */

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

$this->title = 'Обновление последней работы';
$this->params['control'] = [
    'brandLabel' => Html::encode($this->title),
    'items' => [
        [
            'label' => '<span class="glyphicon glyphicon-remove-sign"></span> Отмена',
            'url' => ['last']
        ]
    ]
];

if ($models) {
    $form = ActiveForm::begin([
        'id' => 'gallery-form',
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'validateOnChange' => false
    ]);

        $items = [];
        foreach ($models as $model) {
            $items[$model['id']] = Html::img($model->thumb, ['class' => 'img-responsive']);
        }
        echo Html::radioList('id', '', $items, [
            'encode' => false,
            'class' => 'row',
            'itemOptions' => [
                'container' => [
                    'class' => 'col-sm-2'
                ]
            ]
        ]);

        echo Html::submitButton('Сохранить', [
          'class' => 'btn btn-success pull-right'
        ]);
    ActiveForm::end();
}