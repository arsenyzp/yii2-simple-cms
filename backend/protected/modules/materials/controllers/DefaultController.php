<?php
namespace backend\modules\materials\controllers;

use Yii;
use yii\web\Response;
use yii\web\HttpException;
use yii\widgets\ActiveForm;
use common\modules\materials\models\Material;
use backend\modules\materials\models\search\MaterialSearch;
use backend\modules\admin\components\Controller;
use backend\modules\admin\actions\crud\IndexAction;
use backend\modules\admin\actions\crud\ViewAction;
use backend\modules\admin\actions\crud\CreateAction;
use backend\modules\admin\actions\crud\UpdateAction;
use backend\modules\admin\actions\crud\DeleteAction;
use backend\modules\admin\actions\crud\BatchDeleteAction;
use backend\modules\admin\actions\grid\OrderingAction;
use common\extensions\fileapi\actions\UploadAction as FileAPIUploadAction;
use common\extensions\fileapi\actions\DeleteAction as FileAPIDeleteAction;
use common\extensions\imperavi\actions\UploadAction as ImperaviUploadAction;
use common\extensions\imperavi\actions\GetAction as ImperaviGetAction;

/**
 * Основной контроллер модуля Blogs
 */
class DefaultController extends Controller
{
	/**
	 * @return string Класс основной модели контролера.
	 */
	public function getModelClass()
	{
		return Material::className();
	}

	/**
	 * @return string Класс основной поисковой модели контролера.
	 */
	public function getSearchModelClass()
	{
		return MaterialSearch::className();
	}

	/**
	 * @inheritdoc
	 */
	public function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// Добавляем массив родителей страницы.
			if ($action->id === 'update') {
				$action->params['parentsArray'] = Material::getParentsArray(Yii::$app->request->get('id'));
			}
			return true;
		}
		return false;
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
		    'index' => [
		    	'class' => IndexAction::className(),
		    	'model' => $this->modelClass,
		    	'searchModel' => $this->searchModelClass,
		    	'view' => 'index',
		    	'params' => [
		    		'statusArray' => Material::getStatusArray()
		    	]
		    ],
		    'view' => [
		    	'class' => ViewAction::className(),
		    	'model' => $this->modelClass,
		    	'view' => 'view'
		    ],
		    'create' => [
		    	'class' => CreateAction::className(),
		    	'model' => $this->modelClass,
		    	'view' => 'create',
		    	'scenario' => 'admin-create',
		    	'params' => [
		    		'statusArray' => Material::getStatusArray(),
		    		'parentsArray' => Material::getParentsArray()
		    	]
		    ],
		    'update' => [
		    	'class' => UpdateAction::className(),
		    	'model' => $this->modelClass,
		    	'view' => 'update',
		    	'scenario' => 'admin-update',
		    	'params' => [
		    		'statusArray' => Material::getStatusArray()
		    	]
		    ],
		    'delete' => [
		    	'class' => DeleteAction::className(),
		    	'model' => $this->modelClass
		    ],
		    'batch-delete' => [
		    	'class' => BatchDeleteAction::className(),
		    	'model' => $this->modelClass
		    ],
		    'ordering' => [
		    	'class' => OrderingAction::className(),
		    	'model' => $this->modelClass
		    ],
		    'upload-temp-image' => [
		        'class' => FileAPIUploadAction::className(),
		        'path' => $this->module->imageTempPath(),
		        'types' => $this->module->imageAllowedExtensions
		    ],
		    'delete-temp-image' => [
		        'class' => FileAPIDeleteAction::className(),
		        'path' => $this->module->imageTempPath()
		    ],
		    'imperavi-upload' => [
		    	'class' => ImperaviUploadAction::className(),
		    	'path' => $this->module->contentPath(),
		    	'pathUrl' => $this->module->contentUrl()
		    ],
		    'imperavi-get' => [
		    	'class' => ImperaviGetAction::className(),
		    	'path' => $this->module->contentPath(),
		    	'pathUrl' => $this->module->contentUrl()
		    ]
		];
	}

	/**
	 * Удаляем изображение поста.
	 */
	function actionDeleteImage()
	{
		if ($id = Yii::$app->request->getDelete('id')) {
			$model = $this->findModel($id);
			$model->setScenario('delete-image');
			$model->save(false);
		} else {
			throw new HttpException(400);
		}
	}
}
