<?php
namespace backend\modules\menu\models;

/**
 * Модуль таблицы {{%pages}}
 *
 * @property integer $id ID
 * @property string $parent_id ID родительской страницы
 * @property string $title Заголовок
 * @property string $alias Алиас
 * @property string $content Контент
 * @property boolean $status_id Статус
 * @property integer $create_time Дата создания
 * @property integer $update_time Дата обновления
 */
class Item extends \common\modules\menu\models\Item
{
}