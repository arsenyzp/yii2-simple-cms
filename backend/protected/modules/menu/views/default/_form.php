<?php
/**
 * @var yii\web\View $this
 * @var backend\modules\users\models\User $model
 * @var yii\widgets\ActiveForm $form
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;

$form = ActiveForm::begin([
	'enableClientValidation' => false,
	'enableAjaxValidation' => true,
	'validateOnChange' => false,
    'beforeSubmit' => new JsExpression('function ($form) {tinymce.triggerSave(); return true;}')
]); ?>
<?= $form->field($model, 'title') .
    $form->field($model, 'description')->textarea() .
    $form->field($model, 'status_id')->dropDownList($statusArray) .
    Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Обновить', [
    	'class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right'
    ]);
ActiveForm::end(); ?>