<?php
/**
 * Обновляем категорию
 * @var yii\base\View $this
 * @var backend\modules\blogs\modules\category\models\Category $model
 */

use yii\helpers\Html;
use yii\widgets\Menu;

$this->title = 'Обновить меню: ' . $model['title'];
$this->params['control'] = [
    'brandLabel' => Html::encode($this->title)
];

echo $this->render('_form', [
	'model' => $model,
	'statusArray' => $statusArray
]);