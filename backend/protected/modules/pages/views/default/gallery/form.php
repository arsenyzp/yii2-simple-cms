<?php
use dosamigos\fileupload\FileUploadUI;
?>

<div id="gallery-form-wrapper">

    <div class="panel panel panel-default">
        <div class="panel-heading">Создать галлерею</div>
        <div class="panel-body">
            <?=FileUploadUI::widget([
                'name' => 'gallery-image',
                'url' => ['/pages/default/upload-gallery-image/', 'page-alias' => $pageAlias],
                'gallery' => false,
                'formView' => '@backend/modules/pages/views/default/gallery/file-uploader/form',
                'uploadTemplateView' => '@backend/modules/pages/views/default/gallery/file-uploader/upload',
                'downloadTemplateView' => '@backend/modules/pages/views/default/gallery/file-uploader/download',
                'fieldOptions' => [
                    'accept' => 'image/*'
                ],
                'clientOptions' => [
                    'maxFileSize' => 2 * 2014 * 1024, // 2MB
                    'dropZone' => '#drop-file-zone',
                    'autoUpload' => true,
                ]
            ]);
            ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo \yii\bootstrap\Button::widget([
            'label' => 'Сгенерировать код',
            'options' => ['onclick' => 'generateGalleryCode();']
        ]); ?>
    </div>

    <div id="gallery-code"></div>
</div>

<script>
    function generateGalleryCode()
    {
        var $form = $('#gallery-form-wrapper form').eq(0);

        // если title указан и alt пустой, заполним его значением title
        $form.find('.template-download').each(function() {
            var $tr = $(this);
            var title = $tr.find('input[name="gallery-image-title[]"]').eq(0);
            var alt = $tr.find('input[name="gallery-image-alt[]"]').eq(0);
            if (title.val() && !alt.val()) {
                alt.val(title.val());
            }
        });

        $.post(
            '/pages/default/generate-gallery-code/',
            $form.serialize(),
            function(json) {
                if (json.success) {
                    $('#gallery-code').html(json.galleryCode);
                } else if (json.message) {
                    alert(json.message);
                }
            },
            'json'
        );
    }
</script>