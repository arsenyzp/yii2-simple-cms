<?php
/**
 * Представление страницы всех тэгов.
 * @var yii\base\View $this Представление
 * @var backend\modules\blogs\models\search\PostSearch $searchModel Поисковая модель
 * @var common\modules\blogs\models\Post $dataProvider Дата провайдер
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\grid\CheckboxColumn;
use yii\grid\SerialColumn;
use backend\modules\admin\grid\OrderingColumn;

$this->title = 'Тэги';
$this->params['control'] = [
    'brandLabel' => Html::encode($this->title),
    'gridId' => 'tags-grid'
];

echo GridView::widget([
    'id' => 'tags-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'class' => CheckboxColumn::classname()
        ],
        [
            'class' => OrderingColumn::className(),
            'attribute' => 'ordering'
        ],
        [
            'attribute' => 'name',
            'format' => 'html',
            'value' => function ($model) {
                return Html::a($model['name'], ['view', 'id' => $model['id']]);
            },
        ],
        [
            'class' => ActionColumn::className(),
            'header' => 'Управление'
        ]
    ]
]);