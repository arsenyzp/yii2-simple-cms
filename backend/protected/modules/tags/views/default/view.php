<?php
/**
 * Представление страницы поста.
 * @var yii\base\View $this Представление
 * @var common\modules\blogs\models\Post $model Модель
 */

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\modules\blogs\models\Post;
use common\modules\pages\models\Page;
use common\modules\galleries\models\Image;
use common\modules\materials\models\Material;

$this->title = $model['name'];
$this->params['control'] = [
    'brandLabel' => Html::encode($this->title),
    'modelId' => $model['id']
];

// Собираем список моделей.
$models = '';
if (($posts = $model->getModels(Post::className())->all()) !== null) {
    $models .= Html::tag('h4', 'Посты');
    foreach ($posts as $key => $post) {
        if ($key !== 0) {
            $models .= '<br />';
        }
        $models .= Html::a($post['title'], ['/blogs/default/view', 'id' => $post['id']]);
    }
}
if (($pages = $model->getModels(Page::className())->all()) !== null) {
    $models .= Html::tag('h4', 'Страницы');
    foreach ($pages as $key => $page) {
        if ($key !== 0) {
            $models .= '<br />';
        }
        $models .= Html::a($page['title'], ['/pages/default/view', 'id' => $page['id']]);
    }
}
if (($images = $model->getModels(Image::className())->all()) !== null) {
    $models .= Html::tag('h4', 'Галереи');
    foreach ($images as $key => $image) {
        if ($key !== 0) {
            $models .= '<br />';
        }
        $models .= Html::a(Html::img($image->thumb, ['width' => 100]), ['/galleries/default/view', 'id' => $image->gallery['id']]);
    }
}
if (($materials = $model->getModels(Material::className())->all()) !== null) {
    $models .= Html::tag('h4', 'Материалы');
    foreach ($materials as $key => $material) {
        if ($key !== 0) {
            $models .= '<br />';
        }
        $models .= Html::a(Html::img($material->thumb, ['width' => 100]), ['/materials/default/view', 'id' => $material['id']]);
    }
}

echo DetailView::widget([
	'model' => $model,
	'attributes' => [
	    'id',
	    'name',
        [
            'attribute' => 'models',
            'format' => 'html',
            'value' => $models
        ]
	]
]);