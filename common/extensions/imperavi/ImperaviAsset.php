<?php
namespace common\extensions\imperavi;

use yii\web\AssetBundle;

/**
 * Менеджер ресурсов
 */
class ImperaviAsset extends AssetBundle
{
	public $sourcePath = '@common/extensions/imperavi/assets';
	public $language;
	public $plugins = [];
	public $css = [
		'redactor.css',
		'customize.css'
	];
	public $js = [
	    'redactor.min.js'
	];
	public $depends = [
		'yii\web\JqueryAsset'
	];

	public function registerAssetFiles($view)
	{
		if ($this->language !== null) {
			$this->js[] = 'lang/' . $this->language . '.js';
		}
		if (!empty($this->plugins)) {
			foreach ($this->plugins as $plugin) {
				if ($plugin === 'clips') {
					$this->css[] = 'plugins/' . $plugin . '/' . $plugin . '.css';
				}
				$this->js[] = 'plugins/' . $plugin . '/' . $plugin .'.js';
			}
		}

		parent::registerAssetFiles($view);
	}
}