<?php
namespace common\extensions\imperavi\actions;

use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yii\web\Response;
use common\extensions\imperavi\models\Upload;

/**
 * UploadAction действие для загрузки изображений или файлов с Imperavi Redactor
 */
class GetAction extends Action
{
	/**
	 * @var string Путь к папке в которой будут загружены файлы
	 */
	public $path;

	/**
	 * @var string URL к папке в которой будут загружены файлы
	 */
	public $pathUrl;

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		if ($this->pathUrl === null) {
			throw new InvalidConfigException('The "pathUrl" attribute must be set.');
		} else {
			$this->pathUrl = rtrim($this->pathUrl, '/') . '/';
		}
		if ($this->path === null) {
			throw new InvalidConfigException('The "path" attribute must be set.');
		} else {
			$this->path = FileHelper::normalizePath($this->path) . DIRECTORY_SEPARATOR;
		}
	}

	/**
	 * @inheritdoc
	 */
	public function run()
	{
		$result = $this->getImages($this->path, $this->pathUrl);

		Yii::$app->getResponse()->format = Response::FORMAT_JSON;
		return $result;
	}

	/**
	 * @return Функция сканирует папку и возвращает заполненый массив изображений.
	 */
	protected function getImages($path, $url)
	{
		$images = is_dir($path) ? scandir($path) : [];
		$result = [];

		if (!empty($images)) {
			foreach ($images as $image) {
				if ($image !== '.' && $image !== '..') {
					if (is_dir($path . $image)) {
						$result = array_merge($result, $this->getImages($path . $image . DIRECTORY_SEPARATOR, $url . $image . '/'));
					} else {
						$result[] = [
							'thumb' => $url . $image,
							'image' => $url . $image
						];
					}
				}
			}
		}

		return $result;
	}
}
