<?php
namespace common\extensions\imperavi\actions;

use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yii\web\Response;
use common\extensions\imperavi\models\Upload;

/**
 * UploadAction действие для загрузки изображений или файлов с Imperavi Redactor
 */
class UploadAction extends Action
{
	/**
	 * @var string Название валидатора
	 */
	public $validator = 'image';

	/**
	 * @var array Типы дсотупных расширений файлов для загрузки
	 */
	public $types;

	/**
	 * @var string Путь к папке в которой будут загружены файлы
	 */
	public $path;

	/**
	 * @var string URL к папке в которой будут загружены файлы
	 */
	public $pathUrl;

	/**
	 * @var boolean В случае true будет сгенерирован уникальное имя для загружаемого файла
	 */
	public $unique = true;

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		if ($this->pathUrl === null) {
			throw new InvalidConfigException('The "pathUrl" attribute must be set.');
		} else {
			$this->pathUrl = rtrim($this->pathUrl, '/') . '/';
		}
		if ($this->path === null) {
			throw new InvalidConfigException('The "path" attribute must be set.');
		} else {
			$this->path = FileHelper::normalizePath($this->path) . DIRECTORY_SEPARATOR;
			FileHelper::createDirectory($this->path);
		}
		$this->controller->enableCsrfValidation = false;
	}

	/**
	 * @inheritdoc
	 */
	public function run()
	{
		if (Yii::$app->request->isPost) {
			$model = new Upload(['validator' => $this->validator, 'types' => $this->types]);
			$model->file = UploadedFile::getInstanceByName('file');
			if ($model->validate()) {
				if ($this->unique === true && ($extension = $model->file->getExtension())) {
					$model->file->name = uniqid() . '.' . $extension;
				}
				if ($model->file->saveAs($this->path . $model->file->name)) {
					$result = [
						'filelink' => $this->pathUrl . $model->file->name,
						'filename' => $model->file->name
					];
				} else {
					$result = [
						'error' => 'Не дуалось загрузить файл.'
					];
				}
			} else {
				$result = [
					'error' => $model->getFirstError('file')
				];
			}

			Yii::$app->getResponse()->format = Response::FORMAT_JSON;
			return $result;
		}
	}
}
