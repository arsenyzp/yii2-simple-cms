<?php
namespace common\extensions\select2;

use Yii;
use yii\widgets\InputWidget;
use yii\helpers\Json;
use yii\helpers\Html;
use common\extensions\select2\assets\Select2Asset;
use common\extensions\select2\assets\Select2BootstrapAsset;

/**
 * Виджет [[Select2]]
 * Виджет обёртка для плагина {@link http://ivaynberg.github.io/select2/ Select2}.
 * @var yii\base\Widget $this Виджет
 * 
 * Пример использования:
 * ~~~
 * echo $form->field($model, 'field')->widget(Select2::className(), [
 *     'options' => [
 *         'multiple' => true,
 * 		   'placeholder' => 'Выберите элемент'
 *     ],
 *     'settings' => [
 *         'width' => '100%',
 *     ],
 *     'items' => [
 *         'item1',
 *         'item2',
 *         ...
 *     ]
 * ]);
 * ~~~
 */
class Select2 extends InputWidget
{
	/**
	 * @var array Настройки плагина {@link http://ivaynberg.github.io/select2/#documentation Select2}.
	 */
	public $settings = [];

	/**
	 * @var array Опции выпадающего списка.
	 */
	public $items = [];

	/**
	 * @var string Язык плагина. В случае null, будет использован текущий [[\yii\base\Application::language|язык приложения]].
	 */
	public $language;

	/**
	 * @var boolean Включаем\выключаем "Bootstrap 3" шаблон.
	 */
	public $bootstrap = true;

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();
		// Определяем текущий язык.
		if ($this->language === null) {
			$this->language = strtolower(substr(Yii::$app->language , 0, 2));
		}
	}

	/**
	 * @inheritdoc
	 */
	public function run()
	{
		$this->registerClientScript();
		if ($this->hasModel()) {
			return Html::activeDropDownList($this->model, $this->attribute, $this->items, $this->options);
		} else {
			return Html::dropDownList($this->name, $this->value, $this->items, $this->options);
		}
  	}

  	/**
	 * Регистриурем скрипты виджета.
	 */
	public function registerClientScript()
	{
		$view = $this->getView();
		$selector = '#' . $this->options['id'];
		$settings = Json::encode($this->settings);
		$languageJs = Yii::$app->getAssetManager()->publish('@common/extensions/select2/assets/vendor/select2-3.4.5/select2_locale_' . $this->language . '.js');
		// Регистрируем скрипты виджета.
		if ($this->bootstrap === true) {
			Select2BootstrapAsset::register($view);
		} else {
			Select2Asset::register($view);
		}
		$view->registerJsFile($languageJs[1], [Select2Asset::className()]);
		// Инициализируем плагин виджета.
		$view->registerJs("jQuery('$selector').select2($settings);");
	}
}