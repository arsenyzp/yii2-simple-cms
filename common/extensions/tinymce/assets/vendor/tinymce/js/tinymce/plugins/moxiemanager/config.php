<?php
// General
$moxieManagerConfig['general.debug'] = false;
$moxieManagerConfig['general.language'] = 'ru';

// Filesystem
$moxieManagerConfig['filesystem.rootpath'] = '/home/site/statics/web/content/';
$moxieManagerConfig['filesystem.extensions'] = '*';

// Local filesystem
$moxieManagerConfig['filesystem.local.wwwroot'] = '/home/site/statics/web/';
$moxieManagerConfig['filesystem.local.urlprefix'] = "http://test.ru";

// Upload
$moxieManagerConfig['upload.extensions'] = '*';
$moxieManagerConfig['upload.maxsize'] = '50MB';
$moxieManagerConfig['upload.overwrite'] = false;
$moxieManagerConfig['upload.autoresize'] = false;
$moxieManagerConfig['upload.autoresize_jpeg_quality'] = 100;
$moxieManagerConfig['upload.max_width'] = 9000;
$moxieManagerConfig['upload.max_height'] = 9000;
$moxieManagerConfig['upload.chunk_size'] = '10mb';
$moxieManagerConfig['upload.allow_override'] = '*';

// Authentication
$moxieManagerConfig['authenticator'] = 'SessionAuthenticator';

// SessionAuthenticator
$moxieManagerConfig['SessionAuthenticator.logged_in_key'] = 'isMoximanagerUser';
$moxieManagerConfig['SessionAuthenticator.config_prefix'] = 'moxiemanager';