<?php
@session_start();

/**
 * This class handles MoxieManager Yii2Authenticator.
 */
class MOXMAN_Yii2Authenticator_Plugin implements MOXMAN_Auth_IAuthenticator
{
	public function authenticate(MOXMAN_Auth_User $user) {
		$config = MOXMAN::getConfig();
		$session = MOXMAN_Http_Context::getCurrent()->getSession();

		// The user is authenticated so let them though
		return true;
	}
}

MOXMAN::getAuthManager()->add("Yii2Authenticator", new MOXMAN_Yii2Authenticator_Plugin());
?>