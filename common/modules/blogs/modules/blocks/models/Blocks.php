<?php
namespace common\modules\blogs\modules\blocks\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use common\behaviors\TransliterateBehavior;
use common\behaviors\PurifierBehavior;
use common\modules\blogs\modules\categories\models\query\CategoryQuery;

/**
 * Class Category
 * @package common\modules\blogs\modules\categories\models
 * Модель категорий постов.
 *
 * @property integer $id ID.
 * @property string $alias Алиас.
 * @property string $title Заголовок.
 * @property integer $status_id Статус публикации.
 */
class Blocks extends ActiveRecord
{

	/**
	 * Ключи кэша которые использует модель.
	 */
	const CACHE_CATEGORIES_LIST_DATA = 'blocksListData';

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%blocks}}';
	}


	/**
	 * @return array Массив с статусами постов.
	 */
	public static function getStatusArray()
	{
		return [
		    self::STATUS_UNPUBLISHED => 'Неопубликованно',
		    self::STATUS_PUBLISHED => 'Опубликованно'
		];
	}

	/**
	 * @return string Читабельный статус поста.
	 */
	public function getStatus()
	{
		$status = self::getStatusArray();
		return $status[$this->status_id];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
		    // Общие правила
			[['title_button', 'title', 'link'], 'filter', 'filter' => 'trim'],

			// Заголовок [[title]]
			[ ['title', 'link'], 'required'],
		];
	}


	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
		  'id' => 'ID',
			'title_button' => 'Заголовок кнопки',
			'title' => 'Заголовок',
			'link' => 'Ссылка'
		];
	}

	/**
	 * @inheritdoc
	 */
	public function beforeSave($insert)
	{
		if(parent::beforeSave($insert)) {
			Yii::$app->getCache()->delete(self::CACHE_CATEGORIES_LIST_DATA);
			return true;
		}
		return false;
	}
}