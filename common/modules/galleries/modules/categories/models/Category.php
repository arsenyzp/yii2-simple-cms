<?php
namespace common\modules\galleries\modules\categories\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use common\helpers\TransliterateHelper;
use common\modules\galleries\models\Gallery;
use common\modules\galleries\modules\categories\models\query\CategoryQuery;

/**
 * Общая модель категорий галереи
 *
 * @property integer $id
 * @property string $title
 * @property boolean $status_id
 */
class Category extends ActiveRecord
{
	/**
	 * Статусы записей модели
	 */
	const STATUS_UNPUBLISHED = 0;
	const STATUS_PUBLISHED = 1;

	/**
	 * Ключи кэша которые использует модель
	 */
	const CACHE_CATEGORIES_LIST_DATA = 'galleries_categories_list_data';

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%galleries_categories}}';
	}

	/**
	 * @inheritdoc
	 */
	public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    /**
	 * Выбор записи по [[id]]
	 * @param integer $id
	 */
	public static function findIdentity($id)
	{
		return static::findOne($id);
	}

	/**
	 * Выбор записей по [[id]]
	 * @param array $id
	 */
	public static function findMultipleIdentity($id)
	{
		return static::find()->where(['id' => $id])->all();
	}

	/**
	 * @return array Массив с статусами категорий
	 */
	public static function getStatusArray()
	{
		return [
			self::STATUS_PUBLISHED => 'Опубликованно',
		    self::STATUS_UNPUBLISHED => 'Неопубликованно'
		];
	}

	/**
	 * @return string Читабельный статус категории
	 */
	public function getStatus()
	{
		$status = self::getStatusArray();
		return $status[$this->status_id];
	}

	/**
	 * @return array Массив всех категорий
	 */
	public static function getCategoryArray()
	{
		$key = self::CACHE_CATEGORIES_LIST_DATA;
		$value = Yii::$app->getCache()->get($key);
		if ($value === false || empty($value)) {
			$value = self::find()->select(['id', 'title'])->orderBy('title')->published()->asArray()->all();
			$value = ArrayHelper::map($value, 'id', 'title');
			Yii::$app->cache->set($key, $value);
		}
		return $value ? $value : [];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['title', 'required'],
			['status_id', 'in', 'range' => array_keys(self::getStatusArray())],
			['status_id', 'default', 'value' => self::STATUS_PUBLISHED],
			['title, sub_title', 'match', 'pattern' => '/^[a-zа-яё0-9 _,"\'\\-.\/]+$/iu'],
			['ordering', 'integer', 'integerOnly' => true],
			['ordering', 'default', 'value' => 0]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		return [
			'admin-create' => ['title', 'sub_title', 'ordering', 'status_id'],
			'admin-update' => ['title', 'sub_title', 'ordering', 'status_id']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Заголовок',
      'sub_title' => 'Под заголовок',
			'ordering' => 'Позиция',
			'status_id' => 'Статус'
		];
	}

	/**
	 * @inheritdoc
	 */
	public function afterSave($insert)
	{
		Yii::$app->getCache()->delete(self::CACHE_CATEGORIES_LIST_DATA);
		parent::afterSave($insert);
	}

	/**
	 * @inheritdoc
	 */
	public function beforeDelete()
	{
		if (parent::beforeDelete()) {
			Yii::$app->getCache()->delete(self::CACHE_CATEGORIES_LIST_DATA);
			if ($models = Gallery::find()->where(['category_id' => $this->id])->all()) {
				foreach ($models as $model) {
					$model->delete();
				}
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return \yii\db\ActiveRelation Галереи категории
	 */
	public function getGalleries()
	{
		return $this->hasMany(Gallery::className(), ['category_id' => 'id'])->with('images')->published()->orderBy('ordering ASC');
	}
}