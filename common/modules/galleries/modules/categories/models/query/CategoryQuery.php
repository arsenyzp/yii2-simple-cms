<?php
namespace common\modules\galleries\modules\categories\models\query;

use yii\db\ActiveQuery;
use common\modules\galleries\modules\categories\models\Category;

/**
 * Class CategoryQuery
 * @package common\modules\galleries\modules\categories\models\query
 * Класс кастомных запросов модели [[Category]]
 */
class CategoryQuery extends ActiveQuery
{
	/**
	 * Выбираем только опубликованые записи.
	 * @param ActiveQuery $query
	 */
	public function published()
	{
		$this->andWhere(Category::tableName() . '.status_id = :status', [':status' => Category::STATUS_PUBLISHED]);
		return $this;
	}

	/**
	 * Выбираем только неопубликованные записи.
	 * @param ActiveQuery $query
	 */
	public function unpublished($query)
	{
		$this->andWhere(Category::tableName() . '.status_id = :status', [':status' => Category::STATUS_UNPUBLISHED]);
		return $this;
	}
}