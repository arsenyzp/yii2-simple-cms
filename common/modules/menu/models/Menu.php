<?php
namespace common\modules\menu\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use common\modules\menu\models\query\MenuQuery;

/**
 * Модуль таблицы {{%menu}}
 *
 * @property integer $id ID
 * @property string $title название меню
 * @property string $description Описание
 * @property boolean $status_id Статус
 */
class Menu extends ActiveRecord
{
	/**
	 * Статусы записей модели
	 */
	const STATUS_UNPUBLISHED = 0;
	const STATUS_PUBLISHED = 1;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%menu}}';
	}

	/**
	 * @inheritdoc
	 */
	public static function find()
    {
        return new MenuQuery(get_called_class());
    }

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'purifierBehavior' => [
				'class' => 'common\behaviors\PurifierBehavior',
				'textAttributes' => [
					ActiveRecord::EVENT_BEFORE_UPDATE => ['title', 'description'],
					ActiveRecord::EVENT_BEFORE_INSERT => ['title', 'description'],
				]
			]
		];
	}

	/**
	 * Выбор записи по [[id]]
	 * @param integer $id
	 */
	public static function findIdentity($id)
	{
		return static::findOne($id);
	}

	/**
	 * Выбор записей по [[id]]
	 * @param array $id
	 */
	public static function findMultipleIdentity($id)
	{
		return static::find()->where(['id' => $id])->all();
	}

	/**
	 * @return array Массив с статусами страниц
	 */
	public static function getStatusArray()
	{
		return [
			self::STATUS_PUBLISHED => 'Опубликованно',
		    self::STATUS_UNPUBLISHED => 'Неопубликованно'
		];
	}

	/**
	 * @return array Массив всех доступных родительских путктов меню
	 */
	public static function getMenuArray()
	{
		return ArrayHelper::map(self::find()->all(), 'id', 'title');
	}

	/**
	 * @return string Читабельный статус поста
	 */
	public function getStatus()
	{
		$status = self::getStatusArray();
		return $status[$this->status_id];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title', 'description'], 'string', 'max' => 255],
			['status_id', 'in', 'range' => array_keys(self::getStatusArray())],
			['status_id', 'default', 'value' => self::STATUS_PUBLISHED]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		return [
			// Backend сценарии
			'admin-update' => ['title', 'description', 'status_id'],
			'admin-create' => ['title', 'description', 'status_id']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Название',
			'description' => 'Описание',
			'status_id' => 'Статус'
		];
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getItems($parent = false)
	{
		return $this->hasMany(Item::className(), ['menu_id' => 'id'])->where(['parent_id' => 0])->orderBy('ordering ASC')->with('children');
	}

	/**
	 * @inheritdoc
	 */
	public function beforeDelete()
	{
		if (parent::beforeDelete()) {
			if ($this->items) {
				foreach ($this->items as $item) {
					$item->delete();
				}
			}
			return true;
		} else {
			return false;
		}
	}
}