<?php
namespace common\modules\slider\models\query;

use yii\db\ActiveQuery;
use common\modules\slider\models\Image;

/**
 * Class PostQuery
 * Класс кастомных запросов модели [[Post]]
 */
class ImageQuery extends ActiveQuery
{
	/**
	 * Выбираем только опубликованые записи.
	 * @param ActiveQuery $query
	 * @return ActiveQuery $this
	 */
	public function published()
	{
		$this->andWhere(Image::tableName() . '.status_id = :status', [':status' => Image::STATUS_PUBLISHED]);
		return $this;
	}
}