<?php
$rootDir = dirname(dirname(__DIR__));
Yii::setAlias('root', $rootDir);
Yii::setAlias('common', $rootDir . DIRECTORY_SEPARATOR . 'common');
Yii::setAlias('console', $rootDir . DIRECTORY_SEPARATOR . 'console');
Yii::setAlias('frontend', $rootDir . DIRECTORY_SEPARATOR . 'frontend' . DIRECTORY_SEPARATOR . 'protected');
$params = array_merge(
	require($rootDir . '/common/config/params.php'),
	require($rootDir . '/common/config/params-local.php'),
	require(__DIR__ . '/params.php'),
	require(__DIR__ . '/params-local.php')
);
return [
	'id' => 'app-console',
	'basePath' => dirname(__DIR__),
	'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
	'controllerNamespace' => 'console\controllers',
	'modules' => [
		'galleries' => [
		    'class' => 'frontend\modules\galleries\Galleries'
		]
	],
	'extensions' => require($rootDir . '/vendor/yiisoft/extensions.php'),
	'components' => [
		'db' => $params['components.db'],
		'cache' => $params['components.cache'],
		'mail' => $params['components.mail'],
		'log' => [
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
	],
	'params' => $params,
];
