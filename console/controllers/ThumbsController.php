<?php
namespace console\controllers;

use \Imagick;
use Yii;
use yii\helpers\FileHelper;
use yii\console\Controller;
use common\modules\galleries\models\Gallery;

/**
 * Контроллер для фоновой работы с пользователями.
 * Кончольный контроллер модуля [[Users]]
 */
class ThumbsController extends Controller
{
    /**
     * Создаем мини-изображения в фоновом режиме.
     */
    public function actionGallery($id)
    {
        $model = Gallery::find()->where(['id' => $id])->with('images')->one();
        if ($model !== null && $model->images) {
            $module = Yii::$app->getModule('galleries');
            FileHelper::createDirectory($module->thumbPath($model->id));
            foreach ($model->images as $image) {
                $source = $module->path($model->id, $image->name);
                $thumb = $module->thumbPath($model->id, $image->name);
                $this->ratioThumbnail($source, $thumb, $module->thumbWidth, $module->thumbHeight);
            }
        }
    }

    /**
     * Функция обрезает по укзаным параметрам и сохраняет картинку в указаную папку.
     * @param string $source Путь к исходной картинки.
     * @param string $thumb Путь куда нужно сохранить новый thumbnail.
     * @param integer $width Ширина thumbnail-а.
     * @param integer $height Высота thumbnail-а.
     * @param integer $quality Качество thumbnail-а.
     */
    protected function ratioThumbnail($source, $thumb, $width, $height, $quality = 75)
    {
        $image = new Imagick($source);
        $imgWidth = $image->getImageWidth();
        $imgHeight = $image->getImageHeight();
        $ratio = $imgWidth/$imgHeight;
        $coeficient = 4/3;

        if ($ratio !== $coeficient) {
            if ($imgWidth > $imgHeight) {
                if ($ratio > $coeficient) {
                    $tHeight = $imgHeight;
                    $tWidth = $tHeight * $coeficient;
                } else {
                    $tWidth = $imgWidth;
                    $tHeight = $tWidth * 1/$coeficient;
                }
            } else {
                $tWidth = $imgWidth;
                $tHeight = $tWidth * $coeficient;
            }

            $image->cropThumbnailImage($tWidth, $tHeight);
        }

        $image->resizeImage($width, $height, Imagick::FILTER_LANCZOS, true);

        $image->setImageCompressionQuality($quality);
        $image->stripImage();
        $image->writeImage($thumb);
        $image->destroy();
    }
}