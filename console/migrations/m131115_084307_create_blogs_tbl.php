<?php

use yii\db\Schema;
use yii\db\Migration;
use common\modules\tags\helpers\TagsHelper;
use common\modules\blogs\models\Post;

/**
 * Миграция которая создает все таблицы БД которые относятся к блогам
 */
class m131115_084307_create_blogs_tbl extends Migration
{
	public function up()
	{
		// Настройки MySql таблицы
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

		// Добавляем запись в таблицу моделей
		$modelClass = Post::className();
		$modelClassId = TagsHelper::crcClassName($modelClass);
		$modelClass = addslashes($modelClass);
		$this->execute("INSERT INTO {{%models}} (`model_class`, `model_class_id`) VALUES ('$modelClass', '$modelClassId')");


		// Создаём таблицу блогов
		$this->createTable('{{%posts}}', array(
			'id' => Schema::TYPE_PK,
			'title' => Schema::TYPE_STRING . '(100) NOT NULL',
			'alias' => Schema::TYPE_STRING . '(100) NOT NULL',
			'snippet' => Schema::TYPE_STRING . '(150) NOT NULL',
			'introduction' => 'text NOT NULL',
			'content' => 'longtext NOT NULL',
			'preview_url' => Schema::TYPE_STRING . '(64) NOT NULL',
			'image_url' => Schema::TYPE_STRING . '(64) NOT NULL',
			'author_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'status_id' => 'tinyint(1) NOT NULL DEFAULT 1',
			'create_time' => Schema::TYPE_INTEGER . ' NOT NULL',
			'update_time' => Schema::TYPE_INTEGER . ' NOT NULL',
			'views' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0'
		), $tableOptions);

		$this->createIndex('status_id', '{{%posts}}', 'status_id');
		$this->createIndex('create_time', '{{%posts}}', 'create_time');
		$this->createIndex('update_time', '{{%posts}}', 'update_time');
		$this->createIndex('views', '{{%posts}}', 'views');

		// Создаём таблицу категорий
		$this->createTable('{{%posts_categories}}', [
			'id' => Schema::TYPE_PK,
			'alias' => Schema::TYPE_STRING . '(100) NOT NULL',
			'title' => Schema::TYPE_STRING . '(100) NOT NULL',
			'status_id' => 'tinyint(1) NOT NULL DEFAULT 1',
		], $tableOptions);

		$this->createIndex('alias', '{{%posts_categories}}', 'alias');
		$this->createIndex('status_id', '{{%posts_categories}}', 'status_id');

		// Создаём вспомогательную таблицу post-category
		$this->createTable('{{%post_category}}', [
            'post_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'category_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'PRIMARY KEY (`post_id`, `category_id`)'
        ], $tableOptions);

        $this->addForeignKey('FK_post_category_post_id', '{{%post_category}}', 'post_id', '{{%posts}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('FK_post_category_category_id', '{{%post_category}}', 'category_id', '{{%posts_categories}}', 'id', 'CASCADE', 'CASCADE');

		// Создаём таблицу подписки
		$this->createTable('{{%subscription}}', array(
			'id' => Schema::TYPE_PK,
			'email' => Schema::TYPE_STRING . '(100) NOT NULL',
			'create_time' => Schema::TYPE_INTEGER . ' NOT NULL'
		), $tableOptions);

		$this->createIndex('email', '{{%subscription}}', 'email', true);
	}

	public function down()
	{
		$this->dropTable('{{%post_category}}');
		$this->dropTable('{{%posts}}');
		$this->dropTable('{{%posts_categories}}');
		$this->dropTable('{{%subscription}}');
	}
}
