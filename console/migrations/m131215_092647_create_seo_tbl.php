<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Миграция которая создает все таблицы БД которые относятся к SEO
 */
class m131215_092647_create_seo_tbl extends Migration
{
	public function up()
	{
		// Настройки MySql таблицы
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

		// Создаём таблицу категорий
		$this->createTable('{{%seo}}', array(
			'model_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'model_class_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'seo_title' => Schema::TYPE_STRING . '(70) NOT NULL',
			'meta_description' => Schema::TYPE_STRING . '(156) NOT NULL',
			'layout' => Schema::TYPE_STRING . '(50) NOT NULL',
			'color' => 'tinyint(1) NOT NULL DEFAULT 0',
			'page_class' => Schema::TYPE_STRING . '(50) NOT NULL',
			'css' => Schema::TYPE_STRING . '(255) NOT NULL',
			'js' => Schema::TYPE_STRING . '(255) NOT NULL',
			'PRIMARY KEY (`model_id`, `model_class_id`)'
		), $tableOptions);
	}

	public function down()
	{
		$this->dropTable('{{%seo}}');
	}
}
