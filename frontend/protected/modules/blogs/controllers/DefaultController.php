<?php
namespace frontend\modules\blogs\controllers;

use Yii;
use yii\web\HttpException;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\modules\blogs\models\Post;
use common\modules\blogs\models\Subscription;
use common\modules\blogs\modules\categories\models\Category;
use frontend\modules\site\components\Controller;
use frontend\modules\blogs\widgets\lastpost\LastPost;

/**
 * Основной контроллер модуля [[Blogs]]
 */
class DefaultController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public $layout = '/blogs';

  public $enableCsrfValidation = false;

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
			    'class' => VerbFilter::className(),
			    'actions' => [
			        'subscription' => ['POST']
			    ]
			]
		];
	}

	public function actionIndex()
	{
    if(Yii::$app->request->post('count_page', false)){
      echo LastPost::widget([
          'secondaryTitle' => 'Интересное'
        ]);
      Yii::$app->end();
    }
		$dataProvider = new ActiveDataProvider([
            'query' => Post::find()->with('tags')->innerJoinWith(['categories'])->andWhere([Category::tableName() . '.id' => 1])->published()->orderBy('create_time DESC'),
			'pagination' => [
			    'forcePageParam' => false
			]
		]);

		return $this->render('index', [
			'dataProvider' => $dataProvider
		]);
	}

    public function actionFaq()
    {
        $this->layout = '/faq';
        $dataProvider = new ActiveDataProvider([
            'query' => Post::find()->with('tags')->innerJoinWith(['categories'])->andWhere([Category::tableName() . '.id' => 2])->published()->orderBy('create_time DESC'),
            'pagination' => [
                'forcePageParam' => false
            ]
        ]);

        return $this->render('faq-index', [
                'dataProvider' => $dataProvider
            ]);
    }

	public function actionView($alias)
	{
        if ($model = Post::find()->where(Post::tableName() . '.alias = :alias', [':alias' => $alias])->innerJoinWith(['categories'])->andWhere([Category::tableName() . '.id' => 1])->published()->one()) {
			$model->updateCounters(['views' => 1]);

			return $this->render('view', [
				'model' => $model
			]);
		} else {
			throw new HttpException(404);
		}
	}

    public function actionFaqView($alias)
    {
        $this->layout = '/faq';
        if ($model = Post::find()->where(Post::tableName() . '.alias = :alias', [':alias' => $alias])->innerJoinWith(['categories'])->andWhere([Category::tableName() . '.id' => 2])->published()->one()) {
            $model->updateCounters(['views' => 1]);

            return $this->render('faq-view', [
                    'model' => $model
                ]);
        } else {
            throw new HttpException(404);
        }
    }

	public function actionSubscription()
	{
		$model = new Subscription();
		Yii::$app->response->format = Response::FORMAT_JSON;

		if ($model->load(Yii::$app->request->post())) {
			if ($model->save()) {
				Yii::$app->mail
	    		         ->compose('blogs/subscription', [
	    		         	'email' => $model->email
	    		         ])
	    		         ->setFrom([$model->email])
	    		         ->setTo(Yii::$app->params['adminEmail'])
	    		         ->setSubject('Новый подписчик на сайте ЭтоСервис')
	    		         ->send();
	    		return ['success' => true];
			} elseif (Yii::$app->request->isAjax) {
				return ['errors' => ActiveForm::validate($model)];
			}
		}
	}
}