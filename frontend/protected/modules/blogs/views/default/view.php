<?php
/**
 * Представление одной записи модели
 * @var yii\base\View $this
 * @var frontend\modules\myModule\models\Model $model
 */

use yii\helpers\Html;
use frontend\modules\site\FotoramaAsset;

$this->title = $model['title'];

$this->params['metaDescription'] = $model->seo['meta_description'] ? : $model['title'];
$this->params['metaKeywords'] = $model->seo['meta_keywords'] ? : $model['title'];

// скрипты галлереи
FotoramaAsset::register($this);

?>
<h1 class="headings-border"><a style="text-decoration: none; font-style: normal;" href="/blog/">&larr;</a><a href="/blog/"> <?= $this->title ?></a></h1>
<article id="post">
	<p>
        <time pubdate="<?= $model->createTime ?>"><?= $model->createTime ?></time>
        / <span class="glyphicon glyphicon-eye-open"></span> <?= $model['views'] ?>
        <?php if ($model->tags) {
            $tags = '';
            foreach ($model->tags as $key => $tag) {
                if ($key !== 0) {
                    $tags .= ', ';
                }
                $tags .= Html::a('#' . $tag['name'], ['/search/default/index', 'tag' => $tag['name']]);
            } ?>
            / <?= $tags ?>
        <?php } ?>
    </p>
    <?php if ($model['image_url']) {
    	echo Html::a(Html::img($model->image, ['alt' => $model['title'], 'title' => $model['title']]));
    } ?>
    <?= $model['content']; ?>
</article>
<h2 class="h2_share_margin">Социальные сети:</h2>
<div class="share_div">
<div class="share_btn_div">
<a href="https://twitter.com/share" class="twitter-share-button" data-via="etoservis" data-lang="ru" data-hashtags="моддингiPhone">Твитнуть</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
</div>
<div class="share_btn_div">
<!-- Put this script tag to the <head> of your page -->
<script type="text/javascript" src="//vk.com/js/api/openapi.js?112"></script>

<script type="text/javascript">
  VK.init({apiId: 2442750, onlyWidgets: true});
</script>

<!-- Put this div tag to the place, where the Like block will be -->
<div id="vk_like"></div>
<script type="text/javascript">
VK.Widgets.Like("vk_like", {type: "button", height: 18});
</script>
</div>
</div>
