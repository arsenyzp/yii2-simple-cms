<?php
namespace frontend\modules\blogs\widgets\archive;

use yii\base\Widget;
use common\modules\blogs\models\Post;
use common\modules\blogs\modules\categories\models\Category;

/**
 * Виджет [[Archive]]
 * Выводит последние или популярные новости в зависимости от настроек
 * @var yii\base\Widget $this
 */
class Archive extends Widget
{
	/**
	 * @var string Заголовок виджета
	 */
	public $title;

    /**
     * @var int ИД категории
     */
    public $category = 1;

	/**
	 * @var boolean В случае false будут выводится самые популярные посты
	 * По умолчанею выводятся последние посты
	 */
	public $recent = true;

	public function run()
	{
		$posts = Post::find()->innerJoinWith(['categories'])->andWhere([Category::tableName() . '.id' => $this->category])->orderBy('create_time DESC')->published()->asArray()->all();
		$models = [];

		if ($posts) {
			foreach ($posts as $post) {
				$key = date('Y', $post['create_time']);
				$models[$key][] = $post;
			}
		}
    $cat = Category::find()->andWhere(['id'=>$this->category])->one();
    	return $this->render('index', ['models' => $models, 'cat'=>$cat]);
  	}
}