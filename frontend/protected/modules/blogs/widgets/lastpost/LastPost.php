<?php
namespace frontend\modules\blogs\widgets\lastpost;

use Yii;
use yii\base\Widget;
use common\modules\blogs\models\Post;
use common\modules\blogs\modules\categories\models\Category;

/**
 * Виджет [[News]]
 * Выводит последние или популярные новости в зависимости от настроек
 * @var yii\base\Widget $this
 */
class LastPost extends Widget
{
	/**
	 * @var string Заголовок виджета
	 */
	public $title;

	/**
	 * @var string Заголовок виджета 2-го урованя
	 */
	public $secondaryTitle;

	/**
	 * @var boolean Определяем если нужно показывать ссылку на все посты
	 */
	public $all = true;

	/**
	 * @var boolean В случае false будут выводится самые популярные посты
	 * По умолчанею выводятся последние посты
	 */
	public $recent = true;

    /**
     * @var int ИД категории
     */
  public $category = 1;

	public function run()
	{
		if ($this->recent === true) {
			$field = 'create_time DESC';
		} else {
			$field = 'views DESC';
		}
    $on_page = 30;
    $limit = $on_page;
    if(Yii::$app->request->post('count_page', false)){
      $limit += Yii::$app->request->post('count_page')*$on_page;
    }
		$models = Post::find()->innerJoinWith(['categories'])->andWhere([Category::tableName() . '.id' => $this->category])->offset($limit-$on_page)->limit($limit)->orderBy($field)->all();

    	return $this->render('index', ['models' => $models]);
  	}
}