<?php
namespace frontend\modules\blogs\widgets\subscription;

use yii\base\Widget;

/**
 * Виджет [[Subscription]]
 * Выводит форму подписки на блог сайта
 * @var yii\base\Widget $this
 */
class Subscription extends Widget
{
	/**
	 * @var string Заголовок виджета
	 */
	public $title;

	/**
	 * @inheritdoc
	 */
	public function run()
	{
		$model = new \common\modules\blogs\models\Subscription();
		return $this->render('index', ['model' => $model]);
  	}
}