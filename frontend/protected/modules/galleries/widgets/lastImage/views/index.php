<?php
/**
 * Основной файл представления виджета
 * @var yii\base\View $this
 * @var frontend\modules\myModule\models\Model $model
 */
use yii\helpers\Html;

if ($model !== null) :
	if ($model->tags) {
		$tags = '';
		foreach ($model->tags as $key => $tag) {
			if ($key !== 0) {
				$tags .= ', ';
			}
			$tags .= Html::a('#' . $tag['name'], ['/search/default/index', 'tag' => $tag['name']]);
		}
	} ?>
	<article id="last-image-widget" class="widget">
		<h2><?= 'Фотографии' ?></h2>
		<h1><?= 'Последние работы' ?></h1>
		<h3><a href="<?= $model['last_url'] ?>"><?= $model['title'] ?></a></h3>
		<p>
			<time pubdate="<?= $model->createTime ?>"><?= $model->createTime ?></time>
			<?php if (isset($tags)) { ?>
				<span>/ <?= $tags ?></span>
			<?php } ?>
		</p>
		<?= Html::a(Html::img($model->thumb, ['alt' => $model['title'], 'title' => $model['title']]), $model['last_url']) ?>
	</article>
<?php endif;