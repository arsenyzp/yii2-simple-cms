<?php
namespace frontend\modules\menu\widgets\menu;

use yii\web\AssetBundle;

/**
 * основной пкет приложения
 */
class MenuAsset extends AssetBundle
{
	public $sourcePath = '@frontend/modules/menu/widgets/menu/assets';
	public $js = [
		'js/menu.js'
	];
	public $depends = [
		'yii\web\JqueryAsset'
	];
}