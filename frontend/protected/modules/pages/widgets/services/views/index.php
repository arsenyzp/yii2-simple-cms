<?php
/**
 * Основной файл представления виджета
 * @var yii\base\View $this
 * @var frontend\modules\myModule\models\Model $model
 */
use yii\helpers\Html;

if ($models !== null) : ?>
	<section id="services-widget" class="widget">
		<h2><?= 'Услуги' ?></h2>
		<h1><?= 'Моддинг iPhone' ?></h1>
		<?php foreach ($models as $model) :
			$route = ['/pages/default/view', 'alias' => $model['alias']];
			if ($model->parent_id !== 0) {
				$route['parent'] = $model->parent['alias'];
			} ?>
			<article>
				<h2><?= Html::a($model['title'], $route) ?></h2>
				<?php if ($model->image !== null) {
					echo Html::a(Html::img($model->image, ['alt' => $model['title'], 'title' => $model['title']]), $route);
				} ?>
			</article>
		<?php endforeach; ?>
	</section>
<?php endif; ?>