<?php
/**
 * Представление одной записи модели
 * @var yii\base\View $this
 * @var common\modules\pages\models\Page $model
 */

use yii\helpers\Html;

$tag = Yii::$app->getRequest()->get('tag');

$this->title = $tag . '';
$this->params['pageClass'] = 'search';

$this->params['metaDescription'] = $model['seo']['meta_description'] ? : $model['name'];
$this->params['metaKeywords'] = $model['seo']['meta_keywords'] ? : $model['name'];

?>
<h1>Найдено по тэгу &laquo;<?= $tag ?>&raquo;</h1>

<div class="gallery search-block">
	<h2>В галереях</h2>
	<?php if ($galleries) :
		foreach ($galleries as $gallery) :
			echo Html::a(Html::img($gallery->thumb, ['alt' => $gallery['title'], 'title' => $gallery['title']]), ['/galleries/default/index', '#' => $gallery['id']]);
		endforeach;
	else :
		echo '<span>Ничего не найдено.</span>';
	endif; ?>
</div>

<div class="gallery search-block">
	<h2>В материалах</h2>
	<?php if ($materials) :
		foreach ($materials as $material) :
			echo Html::a(Html::img($material->thumb, ['alt' => $material['title'], 'title' => $material['title']]), ['/materials/default/index', '#' => $material['id']]);
		endforeach;
	else :
		echo '<span>Ничего не найдено.</span>';
	endif; ?>
</div>

<div class="blog search-block">
	<h2>В блогах</h2>
	<?php if ($posts) : ?>
		<ul>
			<?php foreach ($posts as $post) :
				echo '<li>' . Html::a($post['title'], ['/blogs/default/view', 'alias' => $post['alias']]) . '</li>';
			endforeach; ?>
		</ul>
	<?php else :
		echo '<span>Ничего не найдено.</span>';
	endif; ?>
</div>
<div class="page search-block">
	<h2>На страницах сайта</h2>
	<?php if ($pages) : ?>
		<ul>
			<?php foreach ($pages as $page) :
				$route = ['/pages/default/view', 'alias' => $page['alias']];
				if ($page['parent_id'] !== 0) {
					$route['parent'] = $page->parent['alias'];
				}
				echo '<li>' . Html::a($page['title'], $route) . '</li>';
			endforeach; ?>
		</ul>
	<?php else :
		echo '<span>Ничего не найдено.</span>';
	endif; ?>
</div>
