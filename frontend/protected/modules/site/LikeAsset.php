<?php
namespace frontend\modules\site;

use yii\web\AssetBundle;

class LikeAsset extends AssetBundle
{
  public $sourcePath = '@frontend/modules/site/assets';
  public $css = [
    'css/social-likes_flat.css'
  ];
  public $js = [
    'js/social-likes.min.js',
  ];
  public $depends = [
    'frontend\modules\site\AppAsset',
    'yii\web\JqueryAsset'
  ];
}