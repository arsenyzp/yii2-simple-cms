<?php
/**
 * Основной шаблон приложения
 * @var $this \yii\base\View
 * @var $content string
 * @var $params array основные параметры View
 */
?>
<?php $this->beginContent('@frontend/modules/site/views/layouts/layout.php'); ?>
	<!-- Content -->
	<section id="content">
		<!-- Container -->
		<div class="container-fluid">
			<?= $content; ?>
		</div>
		<!--/ Container -->
    </section>
    <!--/ Content -->
<?php $this->endContent(); ?>