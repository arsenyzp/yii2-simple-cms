<?php
/**
 * Основной файл представления виджета
 * @var yii\base\View $this
 * @var frontend\modules\myModule\models\Model $model
 */
use yii\helpers\Html; 
use yii\helpers\Json;
?>
<?php if ($models) : ?>
	<div id="slider-widget" class="widget">
		<?php foreach ($models as $key => $model) : ?>
			<?php $images = [];
			if ($model->images !== null) {
				foreach ($model->images as $image) {
					$images[] = $image->url;
				}
			} ?>
			<article class="slide"<?php if (!empty($images)) { echo ' data-images="' . implode(',', $images) . '"'; } ?>>
				<div class="container">
					<div class="text text-<?= $model['position_id'] ?>">
						<?= $model['content'] ?>
					</div>
				</div>
			</article>
		<?php endforeach; ?>
		<a href="#" class="controls-nav prev"><span>&laquo;</span></a>
		<a href="#" class="controls-nav next"><span>&raquo;</span></a>
	</div>
<?php endif; ?>