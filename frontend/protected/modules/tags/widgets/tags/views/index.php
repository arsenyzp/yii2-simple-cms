<?php
/**
 * Основной файл представления виджета
 * @var yii\base\View $this
 * @var frontend\modules\myModule\models\Model $model
 */
use yii\helpers\Html;

if ($models !== null) : ?>
    <div id="tags-widget" class="widget">
        <h2>Услуги по тегам</h2>

        <p>
            <?php foreach ($models as $key => $model) :
                if ($key !== 0) {
                    echo ', ';
                }
                echo Html::a(
                    '#' . $model['name'],
                    ['/search/default/index', 'tag' => $model['name']],
                    ['title' => $model['name']]
                );
            endforeach; ?>
        </p>
    </div>
<?php endif; ?>
